﻿namespace MyCompany.CircleButtonTest
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Form1 form1 = this;
            form1.circleButton1 = new MyCompany.UserControls.CircleButton(this.components);
            this.circleButton2 = new MyCompany.UserControls.CircleButton(this.components);
            this.circleButton3 = new MyCompany.UserControls.CircleButton(this.components);
            this.SuspendLayout();
            // 
            // circleButton1
            // 
            this.circleButton1.AutoSize = true;
            this.circleButton1.Location = new System.Drawing.Point(54, 77);
            this.circleButton1.Margin = new System.Windows.Forms.Padding(13);
            this.circleButton1.Name = "circleButton1";
            this.circleButton1.Size = new System.Drawing.Size(244, 166);
            this.circleButton1.TabIndex = 0;
            this.circleButton1.Text = "circleButton1";
            this.circleButton1.UseVisualStyleBackColor = true;
            this.circleButton1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.circleButton_KeyPress);
            this.circleButton1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.circleButton1_MouseDown);
            // 
            // circleButton2
            // 
            this.circleButton2.AutoSize = true;
            this.circleButton2.Location = new System.Drawing.Point(404, 86);
            this.circleButton2.Margin = new System.Windows.Forms.Padding(13);
            this.circleButton2.Name = "circleButton2";
            this.circleButton2.Size = new System.Drawing.Size(224, 157);
            this.circleButton2.TabIndex = 1;
            this.circleButton2.Text = "circleButton2";
            this.circleButton2.UseVisualStyleBackColor = true;
            this.circleButton2.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.circleButton_KeyPress);
            this.circleButton2.MouseDown += new System.Windows.Forms.MouseEventHandler(this.circleButton1_MouseDown);
            // 
            // circleButton3
            // 
            this.circleButton3.AutoSize = true;
            this.circleButton3.Location = new System.Drawing.Point(251, 269);
            this.circleButton3.Margin = new System.Windows.Forms.Padding(13);
            this.circleButton3.Name = "circleButton3";
            this.circleButton3.Size = new System.Drawing.Size(224, 157);
            this.circleButton3.TabIndex = 2;
            this.circleButton3.Text = "circleButton3";
            this.circleButton3.UseVisualStyleBackColor = true;
            this.circleButton3.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.circleButton_KeyPress);
            this.circleButton3.MouseDown += new System.Windows.Forms.MouseEventHandler(this.circleButton1_MouseDown);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.circleButton3);
            this.Controls.Add(this.circleButton2);
            this.Controls.Add(this.circleButton1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private MyCompany.UserControls.CircleButton circleButton1;
        private MyCompany.UserControls.CircleButton circleButton2;
        private UserControls.CircleButton circleButton3;
    }
}