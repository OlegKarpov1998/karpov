﻿using MyCompany.UserControls;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace MyCompany.CircleButtonTest
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void circleButton_KeyPress(object sender, KeyPressEventArgs e)
        {
            // Создаем псевдоним переданной ссылки
            CircleButton button = sender as CircleButton;
            if (button != null)
            {
                if (e.KeyChar == ' ')
                {
                    MessageBox.Show("На кнопку " + button.Name + "был нажат пробел");
                }
                else MessageBox.Show("На кнопку " + button.Name + "был нажат клавиша " + e.KeyChar);
            }
        }

        private void circleButton1_MouseDown(object sender, MouseEventArgs e)
        {
            // Создаем псевдоним переданной ссылки
            CircleButton button = sender as CircleButton;
            if (button != null)
                if(e.Button==MouseButtons.Left)
                { 
                    MessageBox.Show("На кнопку " + button.Name + "была нажата левая кнопка мыши"); 
                }
                if (e.Button == MouseButtons.Middle)
                {
                    MessageBox.Show("На кнопку " + button.Name + "была нажата средняя кнопка мыши");
                }
                else MessageBox.Show("На кнопку " + button.Name + "была нажата правая кнопка мыши");
        }
    }
}
