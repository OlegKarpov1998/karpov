﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace IND_LAB2_V2
{
    public partial class Calculate : Form
    {
        public Calculate()
        {
            InitializeComponent();
        }

        private void Calculate_Load(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        // Один обработчик для всех кнопок-операций
        private void btn_Click(object sender, EventArgs e)
        {
            // Копируем текстовые поля в локальные переменные
            string strFirst = string.Copy(txtFirst.Text);
            string strSecond = string.Copy(txtSecond.Text);

            // Замена в строке точки символом запятой
            // для корректного преобразования в число
            int pos = strFirst.IndexOf('.');
            if (pos != -1)
            {
                strFirst = strFirst.Substring(0, pos) + ',' + strFirst.Substring(pos + 1);
            }
            pos = strSecond.IndexOf('.');
            if (pos != -1)
            {
                strSecond = strSecond.Substring(0, pos) + ',' + strSecond.Substring(pos + 1);
            }

            // Преобразуем текст в число для выполнения операций
            if (txtFirst.Text.Length > 0)
                numFirst = Convert.ToDouble(strFirst);
            else
                numFirst = 0.0D;
            if (txtSecond.Text.Length > 0)
                numSecond = Convert.ToDouble(strSecond);
            else
                numSecond = 0.0D;

            // Выполняем нужную операцию
            string btnText = "";// Создали строковую переменную
            bool divideFlag = false;// Флаг деления на ноль
            Button btn = (Button)sender;// Явное приведение типов для распознавания кнопок

            switch (btn.Name)// Переключатель
            {
                case "btnIncrement":// Операция сложения
                    btnText = "\"+\"";// Экраны кавычек
                    numResult = numFirst + numSecond;
                    break;
                case "btnDecrement":// Операция вычитания
                    btnText = "\"-\"";// Экраны кавычек
                    numResult = numFirst - numSecond;
                    break;
                case "btnIncrease":// Операция умножения
                    btnText = "\"*\"";// Экраны кавычек
                    numResult = numFirst * numSecond;
                    break;
                case "btnDivide":// Операция деления
                    btnText = "\":\"";// Экраны кавычек
                                      // Проверяем корректность деления
                    if (Math.Abs(numSecond) < 1.0E-30)
                    {
                        MessageBox.Show(
                        "Делить на ноль нельзя!", // Сообщение
                        "Ошибка", // Заголовок окна
                        MessageBoxButtons.OK, // Кнопка OK
                        MessageBoxIcon.Stop);// Критическая иконка
                        divideFlag = true;
                    }
                    else
                        numResult = numFirst / numSecond;
                    break;
                case "btnResult":
                    if (!divideFlag)
                    {
                        //MessageBox.Show(Convert.ToString(numResult), "Ответ", MessageBoxButtons.OK);
                        txtResult.Text = Convert.ToString(numResult);
                        this.Validate(); // Обновить экран (можно убрать-излишне)
                    }
                    break;
            }

            // Для отображения в панели Output режима Debug
            System.Diagnostics.Debug.WriteLine("Нажата кнопка " + btnText);//Конкатенация


            // Отображение результата //вывод в отдельном окне
            /*if (!divideFlag)
            {
                txtResult.Text = Convert.ToString(numResult);
                this.Validate(); // Обновить экран (можно убрать-излишне)
            }*/
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
