﻿using System.Windows.Forms;

namespace IND_LAB2_V2
{
    partial class Calculate
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.idFirst = new System.Windows.Forms.Label();
            this.idSecond = new System.Windows.Forms.Label();
            this.idResult = new System.Windows.Forms.Label();
            this.txtFirst = new System.Windows.Forms.TextBox();
            this.txtSecond = new System.Windows.Forms.TextBox();
            this.txtResult = new System.Windows.Forms.TextBox();
            this.btnIncrement = new System.Windows.Forms.Button();
            this.btnDecrement = new System.Windows.Forms.Button();
            this.btnIncrease = new System.Windows.Forms.Button();
            this.btnDivide = new System.Windows.Forms.Button();
            this.btnExit = new System.Windows.Forms.Button();
            this.btnResult = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // idFirst
            // 
            this.idFirst.AutoSize = true;
            this.idFirst.Location = new System.Drawing.Point(12, 49);
            this.idFirst.Name = "idFirst";
            this.idFirst.Size = new System.Drawing.Size(77, 13);
            this.idFirst.TabIndex = 0;
            this.idFirst.Text = "Первое число";
            this.idFirst.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // idSecond
            // 
            this.idSecond.AutoSize = true;
            this.idSecond.Location = new System.Drawing.Point(12, 91);
            this.idSecond.Name = "idSecond";
            this.idSecond.Size = new System.Drawing.Size(75, 13);
            this.idSecond.TabIndex = 1;
            this.idSecond.Text = "Второе число";
            this.idSecond.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.idSecond.Click += new System.EventHandler(this.label1_Click);
            // 
            // idResult
            // 
            this.idResult.AutoSize = true;
            this.idResult.Location = new System.Drawing.Point(12, 194);
            this.idResult.Name = "idResult";
            this.idResult.Size = new System.Drawing.Size(59, 13);
            this.idResult.TabIndex = 2;
            this.idResult.Text = "Результат";
            this.idResult.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtFirst
            // 
            this.txtFirst.Location = new System.Drawing.Point(112, 49);
            this.txtFirst.Name = "txtFirst";
            this.txtFirst.Size = new System.Drawing.Size(100, 20);
            this.txtFirst.TabIndex = 3;
            this.txtFirst.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtFirst_KeyDown);
            this.txtFirst.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtFirst_KeyPress);
            // 
            // txtSecond
            // 
            this.txtSecond.Location = new System.Drawing.Point(112, 88);
            this.txtSecond.Name = "txtSecond";
            this.txtSecond.Size = new System.Drawing.Size(100, 20);
            this.txtSecond.TabIndex = 4;
            // 
            // txtResult
            // 
            this.txtResult.Location = new System.Drawing.Point(112, 191);
            this.txtResult.Name = "txtResult";
            this.txtResult.Size = new System.Drawing.Size(100, 20);
            this.txtResult.TabIndex = 5;
            // 
            // btnIncrement
            // 
            this.btnIncrement.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnIncrement.Location = new System.Drawing.Point(112, 132);
            this.btnIncrement.Name = "btnIncrement";
            this.btnIncrement.Size = new System.Drawing.Size(40, 40);
            this.btnIncrement.TabIndex = 6;
            this.btnIncrement.Text = "+";
            this.btnIncrement.UseVisualStyleBackColor = true;
            this.btnIncrement.Click += new System.EventHandler(this.btn_Click);
            // 
            // btnDecrement
            // 
            this.btnDecrement.Font = new System.Drawing.Font("Courier New", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnDecrement.Location = new System.Drawing.Point(187, 135);
            this.btnDecrement.Name = "btnDecrement";
            this.btnDecrement.Size = new System.Drawing.Size(40, 40);
            this.btnDecrement.TabIndex = 7;
            this.btnDecrement.Text = "-";
            this.btnDecrement.UseVisualStyleBackColor = true;
            this.btnDecrement.Click += new System.EventHandler(this.btn_Click);
            // 
            // btnIncrease
            // 
            this.btnIncrease.Font = new System.Drawing.Font("Courier New", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnIncrease.Location = new System.Drawing.Point(264, 135);
            this.btnIncrease.Name = "btnIncrease";
            this.btnIncrease.Size = new System.Drawing.Size(40, 40);
            this.btnIncrease.TabIndex = 8;
            this.btnIncrease.Text = "*";
            this.btnIncrease.UseVisualStyleBackColor = true;
            this.btnIncrease.Click += new System.EventHandler(this.btn_Click);
            // 
            // btnDivide
            // 
            this.btnDivide.Font = new System.Drawing.Font("Courier New", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnDivide.Location = new System.Drawing.Point(347, 135);
            this.btnDivide.Name = "btnDivide";
            this.btnDivide.Size = new System.Drawing.Size(40, 40);
            this.btnDivide.TabIndex = 9;
            this.btnDivide.Text = ":";
            this.btnDivide.UseVisualStyleBackColor = true;
            this.btnDivide.Click += new System.EventHandler(this.btn_Click);
            // 
            // btnExit
            // 
            this.btnExit.Location = new System.Drawing.Point(112, 252);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(75, 23);
            this.btnExit.TabIndex = 10;
            this.btnExit.Text = "Выход";
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // btnResult
            // 
            this.btnResult.Font = new System.Drawing.Font("Courier New", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnResult.Location = new System.Drawing.Point(12, 224);
            this.btnResult.Name = "btnResult";
            this.btnResult.Size = new System.Drawing.Size(40, 40);
            this.btnResult.TabIndex = 11;
            this.btnResult.Text = "=";
            this.btnResult.UseVisualStyleBackColor = true;
            this.btnResult.Click += new System.EventHandler(this.btn_Click);
            // 
            // Calculate
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(415, 330);
            this.Controls.Add(this.btnResult);
            this.Controls.Add(this.btnExit);
            this.Controls.Add(this.btnDivide);
            this.Controls.Add(this.btnIncrease);
            this.Controls.Add(this.btnDecrement);
            this.Controls.Add(this.btnIncrement);
            this.Controls.Add(this.txtResult);
            this.Controls.Add(this.txtSecond);
            this.Controls.Add(this.txtFirst);
            this.Controls.Add(this.idResult);
            this.Controls.Add(this.idSecond);
            this.Controls.Add(this.idFirst);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "Calculate";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "Простой калькулятор";
            this.Load += new System.EventHandler(this.Calculate_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label idFirst;
        private System.Windows.Forms.Label idSecond;
        private System.Windows.Forms.Label idResult;
        private System.Windows.Forms.TextBox txtFirst;
        private System.Windows.Forms.TextBox txtSecond;
        private System.Windows.Forms.TextBox txtResult;
        private System.Windows.Forms.Button btnIncrement;
        private System.Windows.Forms.Button btnDecrement;
        private System.Windows.Forms.Button btnIncrease;
        private System.Windows.Forms.Button btnDivide;
        private System.Windows.Forms.Button btnExit;
        private bool isNumber = false;

        private void txtFirst_KeyDown(object sender,System.Windows.Forms.KeyEventArgs e)
        {
            isNumber = e.KeyCode >= Keys.D0 && e.KeyCode <= Keys.D9 // keyboard -основная клавиатура
            || e.KeyCode >= Keys.NumPad0 && e.KeyCode <= Keys.NumPad9 //keypad - дополнительная клавиатура
            || e.KeyCode == Keys.Back;
        }

        private void txtFirst_KeyPress(object sender,System.Windows.Forms.KeyPressEventArgs e)
        {
            TextBox box = (TextBox)sender;// Явное преобразование типов

            switch (e.KeyChar) // Переключатель
            {
                case '-': // Разрешаем минус, если он первый
                    if (box.Text.Length == 0)
                        isNumber = true;
                    break;
                case '.':
                    // Точка не должна быть первой
                    if (box.Text.Length == 0)
                        break;
                    // Точка не должна следовать сразу за минусом
                    if (box.Text[0] == '-' && box.Text.Length == 1)
                        break;
                    // Точка должна быть одна
                    if (box.Text.IndexOf('.') == -1)
                        isNumber = true; // Еще не было точек
                    break;
            }

            // Запрещаем в текстовом поле лишние символы
            if (!isNumber)
                e.Handled = true;

        }
        // Объявляем числовые переменные как поля-члены класса
        // Их можно объявить как локальные и внутри обработчика btn_Click()
        // Но оставляем область видимости класс, вдруг где-нибудь еще пригодятся!
        private double numFirst, numSecond, numResult;
        private Button btnResult;
    }
}

